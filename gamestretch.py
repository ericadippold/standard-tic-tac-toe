def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, space_number):
    print_board(board)
    print("GAME OVER")
    print(board[space_number], "has won")
    exit()

#def is_row_winner(board, space_number):
    #if board[space_number] == board[space_number + 1] and board[space_number + 1] == board[space_number +2]:
        #return True

#def is_row_winner_two(board, space_number):
    #if board[space_number] == board[space_number +3] and board[space_number + 3] == board[space_number +6]:
        #return True

def top_row_is_winner(board):
    if board[0] == board[1] and board[1] == board[2]:
        return True
def middle_row_is_winner(board):
    if board[3] == board[4] and board[4] == board[5]:
        return True
def bottom_row_is_winner(board):
    if board[6] == board[7] and board[7] == board[8]:
        return True

#aritmetric sequence is +1 and +2

def left_column_is_winner(board):
    if board[0] == board[3] and board[3] == board[6]:
        return True
def middle_column_is_winner(board):
    if board[1] == board[4] and board[4] == board[7]:
        return True
def right_column_is_winner(board):
    if board[2] == board[5] and board[5] == board[8]:
        return True

#arithmetic sequence is +3 and +3

def left_diagonal_is_winner(board):
    if board[0] == board[4] and board[4] == board[8]:
        return True
def right_diagonal_is_winner(board):
    if board[2] == board[4] and board[4] == board[6]:
        return True

#arithmetric sequence is ??

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if top_row_is_winner(board):
        game_over(board, 0)
    elif middle_row_is_winner(board):
        game_over(board, 3)
    elif bottom_row_is_winner(board):
        game_over(board, 6)
    elif left_column_is_winner(board):
        game_over(board, 0)
    elif middle_column_is_winner(board):
        game_over(board, 1)
    elif right_column_is_winner(board):
        game_over(board, 2)
    elif left_diagonal_is_winner(board):
        game_over(board, 0)
    elif right_diagonal_is_winner(board):
        game_over(board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
